package com.example.miguelparra.timediviner
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast //es para usar mensajes de notificacion
import android.util.Log
import kotlinx.android.synthetic.main.activity_game_main.*
import java.util.*

class GameMainActivity : AppCompatActivity() {
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var startGameButton: Button
    internal lateinit var guessButton: Button
    internal lateinit var countDownTimer : CountDownTimer //para trabajar de 0-10 segundos
    internal lateinit var randonNumberTextView: TextView
    //internal val timeLeft = 1000L;
    internal var timeLeft=10 //j usado para imprimir en pantalla
    internal var score = 0
    internal var random = 0
    internal var gameStarted = false //usada para almacenar el estado del juego
    internal var countDownInterval = 1000L //intervalo en que se va ir disminuyendo
    internal var initialCountDown = 10000L //valor inicial del tiempo
    internal var TAG = GameMainActivity::class.java.simpleName //almacenar el nombre en la clase en la que estoy


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_main)

        //connect view to variables
        gameScoreTextView=findViewById(R.id.game_score_text_view)
        startGameButton=findViewById(R.id.start_game)
        guessButton=findViewById(R.id.guess_button)
        randonNumberTextView=findViewById(R.id.random_number_text_view)

        //al dar clic realiza una accion
        guessButton.setOnClickListener{_->(sumarScore())}
        startGameButton.setOnClickListener{_->(empezarJuego())}
        resetGame()

    }



    //J funcion para inicializar todas las cosas y el timer , es decir setear todos a sus valores iniciales
    private fun resetGame(){
        val mostrarRandom= getString(R.string.random_number,Integer.toString(random))
        randonNumberTextView.text=mostrarRandom

        val gameScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text=gameScore

        val restoredTime=getString(R.string.time_left,Integer.toString(timeLeft))


        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            //inicializando un objeto de tipo countDownTimer
            override fun onTick(millisUntilFinished: Long) { //59,58,57,56
                timeLeft = millisUntilFinished.toInt()/1000
                //timeLeftTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
            }
            override fun onFinish() {
                endGame()
            }
        }
        gameStarted=false
    }




    private fun empezarJuego(){
        countDownTimer.start()
        asignarRandom()
        gameStarted = true

    }


    private fun asignarRandom(){
        var r = Random()
        var random = r.nextInt(10 - 0);
        random_number_text_view.text= getString(R.string.random_number,Integer.toString(random))
    }


    private fun sumarScore() {
        val diferencia =  timeLeft-random;
        if (diferencia == 0) {
            score+=100

        } else if (diferencia == 1 || diferencia == -1 ) {
            score += 50

        }
        gameScoreTextView.text=getString(R.string.your_score, Integer.toString(score))
       // countDownTimer.cancel()
       // endGame()
    }



    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over_message, Integer.toString(score)), Toast.LENGTH_LONG).show()
        resetGame()
    }



}
